// Fetch Request Load Chuck Norris Joke Project

// Elements
const jokeCta = document.querySelector("#loadNew");
const output = document.querySelector("#output");

// API URL
const url = `https://api.chucknorris.io/jokes/random`;

// Event Listener for Joke CTA
jokeCta.addEventListener("click", (e) => {
	if (output.innerHTML !== "") {
		output.innerHTML = "";
	}
	fetch(url)
		.then((res) => {
			if (res.status === 200) {
				return res.json();
			}
		})
		.then((data) => {
			renderJoke(data);
		})
		.catch((error) => {
			console.log(error);
		});
});

// Function to render joke
const renderJoke = function (data) {
	output.innerHTML = `
    <img src="${data.icon_url}" alt="chuck norris icon">
    <h2>${data.value}</h2>
    `;
};
