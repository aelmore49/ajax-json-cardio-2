// Fetch Form Post Project

// Elements
const myForm = document.querySelector("#myForm");
const outPut = document.querySelector("#output");

// Event Listner for form
myForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let data = formData(myForm);
	loadData(data);
});

// Function that handles form data
const formData = function (data) {
	let myData = "";
	let element = data.querySelectorAll("input").forEach((el) => {
		if (el.type !== "submit") {
			if (el.value !== "") {
				myData += `${el.name}=${el.value}&`;
			} else {
				alert(`Please fill out the ${el.placeholder} field`);
			}
		}
	});
	return myData.slice(0, -1);
};

// Function that acts like server and loads the form data
const loadData = function (data) {
	const myData = data;
	const method = {
		method: "POST",
		headers: {
			"Content-type": "application/x-www-form-urlencoded;charset=UTF-8",
		},
		body: myData,
	};
	const url = `http://s179017901.onlinehome.us/discoveryvip/`;

	// Setup Fetch Post Reqeust
	fetch(url, method)
		.then((res) => {
			return res.json();
		})
		.then((data) => {
			console.log(data);
		})
		.catch((error) => {
			console.log(error);
		});
};
