// AXIOS Get Data Project

// Elements
const myForm = document.querySelector("#myForm");
const output = document.querySelector("#output");

// Form Event Lister for submit
myForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let url = `https://randomuser.me/api/`;

	// Get request using AXIOS library
	axios.get(url).then((res) => {
		console.log(res.data.results);
		const userData = res.data.results;
		loadData(userData);
	});
});

// Function that loads the returned data
const loadData = function (data) {
	const firstName = data[0].name.first;
	const lastName = data[0].name.last;

	const fields = myForm.querySelectorAll("input");
	fields.forEach((field) => {
		if (field.name === "first") {
			field.value = firstName;
		}
		if (field.name === "last") {
			field.value = lastName;
		}
	});
	output.innerHTML = `<div><h2>Fetched User: ${firstName} ${lastName}</h2></div>`;
};
