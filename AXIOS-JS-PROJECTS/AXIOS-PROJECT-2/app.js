// AXIOS Post Project

// Elements
const myForm = document.querySelector("#myForm");
const output = document.querySelector("#output");

// Form Event Lister for submit
myForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let url = `http://s179017901.onlinehome.us/discoveryvip/`;
	let first = document.querySelector('input[name="first"]').value;
	let last = document.querySelector('input[name="last"]').value;
	let myData = `first=${first}&last=${last}`;
	// POST request using AXIOS library
	axios
		.post(url, myData)
		.then((res) => {
			console.log(res);
		})
		.catch((error) => {
			console.log(error);
		});
});
