// First Project using AXIOS and JS

// Elements
const myForm = document.querySelector("#myForm");
const output = document.querySelector("#output");

// Form Event Lister for submit
myForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let url = `https://randomuser.me/api?results=10`;

	// Get request using AXIOS library
	axios.get(url).then((res) => {
		console.log(res.data.results);
		const userData = res.data.results;
		renderUserData(userData);
	});
});

// Function the renders user data to the DOM
const renderUserData = function (data) {
	data.forEach((user) => {
		console.log(user.name.first);
		myForm.style.display = "none";
		const firstName = user.name.first;
		const lastName = user.name.last;
		const gender = user.gender;
		const email = user.email;
		const picture = user.picture.large;
		const age = user.dob.age;
		output.innerHTML += `
        <div>
            <img src="${picture}" alt="${firstName}-${lastName}">
            <h3> Name: ${firstName} ${lastName} </h3>
            <h3> Gender: ${gender}</h3>
            <h3> Age: ${age}</h3>
            <h3> Email: ${email}</h3>
            <hr>
        </div>
        `;
	});
};
