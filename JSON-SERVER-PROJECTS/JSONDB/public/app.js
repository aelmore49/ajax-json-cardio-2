// JSON SERVER PROJECT 1

// Elements
const btn = document.querySelector("#load1");
const btnPost = document.querySelector("#post1");
const output = document.querySelector("#message");

// API URL
const baseUrl = "http://localhost:3000/";
// Event listner for load data cta
btn.addEventListener("click", (e) => {
	// Call function to make request
	makeFetchRequest(`${baseUrl}posts`);

	// Call function to make XHR request
	//makeXhrRequest(`${baseUrl}posts`, "GET");
});

// Event listener for post data cta
btnPost.addEventListener("click", (e) => {
	// Call function to make XHR post request
	makeXhrRequest(
		`${baseUrl}posts`,
		"POST",
		`title=Doe&author=json&company=VML`
	);
});

// Function that makes fetch reqeust
const makeFetchRequest = function (url, type = "") {
	if (type === "") {
		fetch(url)
			.then((res) => {
				return res.json();
			})
			.then((data) => {
				console.log(data);
			})
			.catch((error) => {
				console.log(error);
			});
	} else if (type !== "") {
		console.log("send post here");
	}
};

// Function that makes a XHR reqeust
const makeXhrRequest = function (url, type, data) {
	const xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			const data = JSON.parse(xhr.responseText);
			data.forEach((d) => {
				const id = d.id;
				const title = d.title;
				const author = d.author;
				const company = d.company;
				output.innerHTML += `${id} - ${title} - ${author} - ${company}`;
			});
		}
	};

	xhr.open(type, url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send(data);
};
