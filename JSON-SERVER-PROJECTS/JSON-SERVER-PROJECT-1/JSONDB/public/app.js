// JSON SERVER PROJECT 1

// Elements
// Page CTAs
const searchCTA = document.querySelector("#search");
const addCommentCTA = document.querySelector("#addComment");
const prevCTA = document.querySelector("#prev");
const nextCTA = document.querySelector("#next");
// Elements that receive and output content
const message = document.querySelector("#message");
const pageInfo = document.querySelector("#pageInfo");
const output = document.querySelector("#output");
const myForm = document.querySelector("#myForm");
// Variables for posts
let storedPosts;
let lastPost;
const defaultPost = `Add Summary To Post`;
const addSummaryDiv = document.querySelector(".comments-container");
// Variables for comments
let storedComments;

// Event listener that listens for a custom "update" event so it can update the post variables accordingly
window.addEventListener(
	"update",
	function (evt) {
		lastPost = evt.detail.lastPost;
		renderData(evt.detail.data);
	},
	false
);
// Event listener that listens for a custom "updateComments" event so it can update the given post with the comments that the user newly added to it
window.addEventListener(
	"updateComments",
	function (evt) {
		renderData(evt.detail.data);
	},
	false
);

// Global currentId variable that is set to 1
let currentId = 1;

// API URL
const url = `http://localhost:3000/posts`;

// Function that sends a XHR GET request
const xhrGetRequest = function (url, type, total = "") {
	// Setup XHR GET request
	// If total param is blank, get a specific post by its id
	if (total === "") {
		const xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.status === 200 && xhr.readyState === 4) {
				const data = JSON.parse(xhr.responseText);
				// Call function to render data to the DOM
				renderData(data);
			}
		};
		xhr.open(type, url, true);
		xhr.send();
	} else if (total !== "") {
		// If total param is not blank, get all of the posts
		const xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function () {
			if (xhr.status === 200 && xhr.readyState === 4) {
				const data = JSON.parse(xhr.responseText);

				// If request is for Posts, Store all posts in local storage posts and initialize storedPosts variable with it.

				// Else If request is for Comments, Store all comments in local storage comments item and initialize storedComments variable with it.
				if (url.includes("posts")) {
					localStorage.setItem("user-posts", xhr.responseText);
					storedPosts = localStorage.getItem("user-posts");
				} else if (url.includes("comments")) {
					localStorage.setItem("comments", xhr.responseText);
					storedComments = localStorage.getItem("comments");
				}
				// Update last and first posts values
				let lastPostStored = JSON.parse(storedPosts).at(-1).id;
				let firstPost = JSON.parse(storedPosts);
				// Create a custom event and pass it the updated first and last post variables within its detail object
				const updateData = new CustomEvent("update", {
					detail: {
						lastPost: lastPostStored,
						data: firstPost[0],
						storage: storedPosts,
					},
				});
				// Dispatch the above update event
				window.dispatchEvent(updateData);
			}
		};
		xhr.open(type, url, true);
		xhr.send();
	}
};

// Funtion that send a XHR POST Request
const xhrPostRequest = function (url, type, data) {
	// Create XHR object for POST request
	const xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			const data = JSON.parse(xhr.responseText);
			// Check if storedPosts has a value
			if (storedPosts) {
				// Parse storedPost value
				let postArray = JSON.parse(storedPosts);
				// Add the value that was just posted to the postArray
				postArray.push(data);
				// update local storage with the stringified postArray
				localStorage.setItem("user-posts", JSON.stringify(postArray));
				// Re-initialize the storedPosts variable to the updated local storage value
				storedPosts = localStorage.getItem("user-posts");
				// Update the last and first posts values
				let lastPostStored = JSON.parse(storedPosts).at(-1).id;
				let firstPost = JSON.parse(storedPosts);
				// Create a custom event and pass it the updated first and last post variables in its detail object
				const updateData = new CustomEvent("update", {
					detail: {
						lastPost: lastPostStored,
						data: firstPost[0],
					},
				});
				// Dispatch the above update event
				window.dispatchEvent(updateData);
			}
		}
	};
	xhr.open(type, url, true);
	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr.send(data);
};

// Function that renders returned data to the DOM
const renderData = function (data) {
	// Check if the output element has content, if it has content, clear the content before rendering the new content to it
	if (output.innerHTML !== "") {
		output.innerHTML = "";
	}
	// Pull out author,title and id properities from the received data object and store them each to constants
	const author = data.author;
	const title = data.title;
	const id = data.id;
	// Set output element's inner html to new received data and render it to the DOM
	output.innerHTML = `<h2>Page ${id} of ${lastPost}
						<h2>Title: ${title} </h2>
						<h2>Author: ${author} </h2>
						<h2 id="post-id"data-id="${id}">ID: ${id} </h2>`;
	loadComments(id);
};
// Event Listener for the Form Submit CTA
myForm.addEventListener("submit", (e) => {
	// Prevent submission on click
	e.preventDefault();
	// Get all of the form elements input values and store them in an array variable
	let formInputs = Array.from(myForm.getElementsByTagName("input"));
	// Empty array for storying necessary post elements
	let postInputs = [];
	// Loop through all the formInputs array, check for empty inputs on all non submit input elements
	formInputs.forEach((i) => {
		if (i.type !== "submit") {
			if (i.value !== "") {
				// Push all non empty, non submit input elements to the postInputs array
				postInputs.push(i);
			} else {
				alert(
					`You left the ${i.name} field blank. Please fill out all of the fields.`
				);
			}
		}
	});
	/*
	If posInputs array has two elements, author input and title input,
	get their values and setup a constant that will be used for making a post request
	 */
	if (postInputs.length === 2) {
		let inputTitle = postInputs[0].value;
		let inputAuthor = postInputs[1].value;
		const postData = `title=${inputTitle}&author=${inputAuthor}`;
		// Send post request, passing it the required post URL, the request type (POST) and the above postData constant
		xhrPostRequest(url, "POST", postData);
	}
	// Call clearFormFields function to clear out all the non-submit input field values
	clearFormFields();
});

// Event Listener for the Previous Post CTA
prevCTA.addEventListener("click", (e) => {
	// If currentId global variable greater than or equal to 2, create prevId variable and set currentId global variable to it.
	if (currentId >= 2) {
		let prevId = currentId - 1;
		currentId = prevId;

		// Went back and made the prevCTA use localStorage Data for getting the previous post, instead of getting the previous post via sending out another GET Request

		let data = JSON.parse(storedPosts);
		let foundId = data.filter((id) => {
			return id.id === currentId;
		});
		if (foundId.length > 0) {
			renderData(foundId[0]);
		}

		// Below is the way to get the previous post via a GET Request
		// Send Get request, using currentId variable as the post id param in the get request's URL
		//xhrGetRequest(`http://localhost:3000/posts/${currentId}`, "GET");

		// Check to see if nextCTA element contains disabled css class, if so, remove it.
		if (nextCTA.classList.contains("disabled")) {
			nextCTA.classList.remove("disabled");
		}
		//If currentId global variable is equal or less than 2, add disabled css class to the prevCTA element and set its disabled attribute to true
	} else if (currentId <= 2) {
		prevCTA.classList.add("disabled");
		prevCTA.disabled = true;
		return;
	}
});
// Event Listener for the Next Post CTA
nextCTA.addEventListener("click", (e) => {
	// If currentId global variable is less than the lastPost global variable, create nextId variable and set currentId variable to it.
	if (currentId < lastPost) {
		let nextId = currentId + 1;
		currentId = nextId;

		// Went back and made the nextCTA use localStorage Data for getting the next post, instead of getting the next post via sending out another GET Request

		// Set data variable to a parsed storedPosts global variable
		let data = JSON.parse(storedPosts);
		let foundId = data.filter((id) => {
			return id.id === currentId;
		});
		if (foundId.length > 0) {
			renderData(foundId[0]);
		}

		// Below is the way to get the next post via a GET Request
		// Send Get request, using currentId variable as the post id param in the get request's URL
		//xhrGetRequest(`http://localhost:3000/posts/${currentId}`, "GET");

		// Check to see if prevCTA element contains disabled css class, if so, remove it.
		if (prevCTA.classList.contains("disabled")) {
			prevCTA.classList.remove("disabled");
		}
		// If currentId global variable is greater than lastPost global variable, add disable css class to it and set its disabled attribute to true
	} else if (currentId >= lastPost) {
		nextCTA.disabled = true;
		nextCTA.classList.add("disabled");
		return;
	}
});

// Event Listener for the Search CTA
searchCTA.addEventListener("click", (e) => {
	// Get search's elements value and set it to searchInput variable
	let searchInput = document.getElementsByName("search")[0].value;
	// Set data variable to a parsed storedPosts global variable
	let data = JSON.parse(storedPosts);
	// Check to see if searchInput variable is not blank
	if (searchInput !== "") {
		// Get search's elements input value,parse it, and set it to the searchText variable
		let searchText = parseInt(document.getElementsByName("search")[0].value);
		// Create foundId array variable, which is just a filtered version of the data variable
		const foundId = data.filter((id) => {
			return id.id === searchText;
		});
		// If foundId constant has length greater than one, pass its element's value to the renderData function so it can be rendered to the DOM.
		if (foundId.length > 0) {
			document.getElementsByName("search")[0].value = "";
			currentId = foundId[0].id;
			renderData(foundId[0]);
		} else {
			alert(
				`There are no posts with an id of "${searchText}". Please enter a different post id to search for.`
			);
			document.getElementsByName("search")[0].value = "";
		}
	} else {
		alert("Please enter a post id to search for");
	}
});

// Event Listener for the Add Comment CTA
addCommentCTA.addEventListener("click", (e) => {
	const postId = +document.getElementById("post-id").getAttribute("data-id");
	let textAreaValue = document.querySelector("#comments").value;
	if (textAreaValue !== "") {
		// Send XHR Post Request to add comment to post
		const type = "POST";
		const postCommentUrl = `http://localhost:3000/comments`;
		const commentData = `id=${postId}&body=${textAreaValue}&postId=${postId}`;
		const xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				const data = JSON.parse(xhr.responseText);
				// Check if storedPosts has a value

				if (storedComments) {
					// Parse storedPost value
					let commentsArray = JSON.parse(storedComments);
					// Add the value that was just posted to the postArray
					commentsArray.push(data);
					// update local storage with the stringified commentsArray
					localStorage.setItem("comments", JSON.stringify(commentsArray));
					// Re-initialize the storedComments variable to the updated local storage value
					storedComments = localStorage.getItem("comments");
					// Create postArray variable from local storage user-post item
					const postArray = JSON.parse(localStorage.getItem("user-posts"));
					// Pull out current post from postArray constant to use for the update comments custom event that follows
					const currentPost = postArray.find((e) => e.id === postId);
					// Create a custom event and pass it the updated post with its newly added comments
					const updateData = new CustomEvent("updateComments", {
						detail: {
							data: currentPost,
						},
					});
					// Dispatch the above update event
					window.dispatchEvent(updateData);
				}
			}
		};
		xhr.open(type, postCommentUrl, true);
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.send(commentData);
		// Call clearFormFields function and pass true to it so it clears the textArea field
		clearFormFields(true);
	} else {
		alert(`Please enter a summary for Post Number ${postId}`);
	}
});

// Function to clear form field values and textArea
// If function receives a true value, it means it needs to clear the textArea
const clearFormFields = function (textArea = false) {
	if (textArea === false) {
		let formInputs = Array.from(myForm.getElementsByTagName("input"));
		formInputs.forEach((i) => {
			if (i.type !== "submit") {
				i.value = "";
			}
		});
	} else if (textArea === true) {
		let textArea = document.querySelector("#comments");
		if (textArea.value !== "") {
			textArea.value = "";
		}
	}
};

// Function to load the post's comments
const loadComments = function (postid) {
	let comments = document.createElement("h3");
	comments.innerHTML = "";
	// Check local storage item storedComments
	if (storedComments) {
		const commentData = JSON.parse(storedComments);

		if (
			commentData.findIndex((e) => e.id.toString() === postid.toString()) < 0
		) {
			comments.innerHTML = defaultPost;
			output.appendChild(comments);
			checkSummary(comments);
		} else {
			commentData.forEach((e) => {
				if (e.id.toString() === postid.toString()) {
					comments.innerHTML = `Summary: ${e.body}`;
					output.appendChild(comments);
					checkSummary(comments);
				}
			});
		}
	}

	// Below is an alternative way of rendering the comments by sending out an http GET request for the comment data

	// Setup and initialize constants for loading the Post's comments to the DOM
	// const type = `GET`;
	// const commentsUrl = `${url}/${postid}/comments`;

	// Send XHR Get Request for comments
	// const xhr = new XMLHttpRequest();
	// xhr.onreadystatechange = function () {
	// 	if (xhr.status === 200 && xhr.readyState === 4) {
	// 		const data = JSON.parse(xhr.responseText);
	// 		// Check if data array has comments object
	// 		const commentsObject = data[0];
	// 		if (commentsObject) {
	// 			// Check if body property of comments object has content
	// 			// If body property has content, render it to the DOM
	// 			const commentsBody = data[0].body;
	// 			if (commentsBody !== "" || commentsBody !== undefined) {
	// 				if (comments.innerHTML !== "") {
	// 					comments.innerHTML = "";
	// 				}
	// 				comments.innerHTML = `Summary: ${commentsBody}`;
	// 				if (output.querySelector("h3") == null) {
	// 					output.appendChild(comments);
	// 					checkSummary(comments);
	// 				}
	// 			}
	// 			// If data array has no comments object, display default comments message to the DOM
	// 		} else {
	// 			if (comments.innerHTML !== "") {
	// 				comments.innerHTML = "";
	// 			}
	// 			comments.innerHTML = defaultPost;
	// 			if (output.querySelector("h3") == null) {
	// 				output.appendChild(comments);
	// 				checkSummary(comments);
	// 			}
	// 		}
	// 	}
	// };
	// xhr.open(type, commentsUrl, true);
	// xhr.send();
};

// Function to check if a Post already has a Summary
const checkSummary = function (comments) {
	const summary = comments;
	const summaryContent = summary.innerHTML;
	if (summaryContent === defaultPost) {
		summary.classList.add("add-summary");
		if (addSummaryDiv.classList.contains("hide")) {
			addSummaryDiv.classList.remove("hide");
		}
	} else {
		addSummaryDiv.classList.add("hide");
	}
};

// Window event listener to load content afte page load
window.addEventListener("DOMContentLoaded", (e) => {
	xhrGetRequest(`http://localhost:3000/posts`, "GET", "all");
	xhrGetRequest(`http://localhost:3000/comments`, "GET", "all");
	xhrGetRequest(`http://localhost:3000/posts/${currentId}`, "GET");
});
