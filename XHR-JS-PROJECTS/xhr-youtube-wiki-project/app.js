// AJAX API YouTube-Wiki Project

// Elements
const searchCta = document.querySelector(".search-cta");
const searchInput = document.querySelector(".search-input");
const outputDiv = document.querySelector(".output-div");
const wikiOutput = document.querySelector(".wiki-output");
const youtubeOutput = document.querySelector(".youtube-output");
const searchTerm = document.querySelector(".search-term");
let globalsearchValue;

// Variable and Constants for API
let displayNumber = 5;
const maxResultsAllowed = 25;

// API URLs
const wikiUrl = `https://en.wikipedia.org/api/rest_v1/page/summary/`;
const youTubeUrl = `https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults=${displayNumber}&q=`;
// API Key
const youTubeKey = `AIzaSyAvw5_XUWYKry_X8UzfYs_XnxTom8CmOb4`;

// Event Listener for the search CTA
searchCta.addEventListener("click", (e) => {
	if (!outputDiv.classList.contains("hide")) {
		outputDiv.classList.add("hide");
	}
	if (globalsearchValue !== "") {
		globalsearchValue = "";
	}
	if (wikiOutput.innerHTML !== "" || youtubeOutput.innerHTML !== "") {
		wikiOutput.innerHTML = "";
		youtubeOutput.innerHTML = "";
	}
	if (searchInput.value !== "") {
		globalsearchValue = searchInput.value;
		const value = searchInput.value.toString().toLowerCase();
		const tempKey = `&key=${youTubeKey}`;
		const tempYouTubeUrl = `${youTubeUrl}${value}${tempKey}`;
		const tempWikiUrl = `${wikiUrl}${searchInput.value}`;
		postRequest(tempWikiUrl, true);
		postRequest(tempYouTubeUrl, false);
		searchInput.value = "";
	} else {
		alert(`Please enter a search term!`);
	}
});

// Function that sends a XHR Post Request
const postRequest = function (url, isWiki) {
	const xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			const parsedData = JSON.parse(xhr.responseText);
			renderData(parsedData, isWiki);
		}
		return;
	};

	xhr.open("GET", url, true);
	xhr.send();
};

// Funtion to render data to the DOM
const renderData = function (data, isWiki) {
	outputDiv.classList.remove("hide");
	searchTerm.innerHTML = `Results for: ${globalsearchValue}`;
	if (isWiki) {
		wikiOutput.innerHTML = `<h2>Wikipedia Results</h2>
        <p>${data.title} - ${data.description}</p>
        <p>${data.extract}</p>
        <p> <a href="${data.content_urls.desktop.page}" target="_blank">Visit ${data.title} page</a></p> <hr>`;
	} else {
		const videoArray = data.items;
		youtubeOutput.innerHTML += `<h2>YouTube Results</h2>`;
		videoArray.forEach((video, index) => {
			//Pull out video id for iframe src attribute
			const { id, snippet } = video;
			const { videoId } = id;
			const { title, description } = snippet;

			// Set iframe src attribute
			const src = `https://www.youtube.com/embed/${videoId}`;
			// Make infoContainer div element's innerhtml iframes of return search results
			youtubeOutput.innerHTML += `<hr class="video-hr">
        <h2><i>Result ${index + 1} of ${videoArray.length}</i></h2>
<h2>Video Title: <i style="color:#a53b3b;">${title}</i></h2>
<h3>Video Description:<i style="color:#a53b3b;"> ${description}</i></h3>
        <div class="video-holder"> <iframe class="video" width="70%" height="400" src="${src}"></iframe></div>
        `;
		});
	}
};
