// AJAX XHR Load Local File Project

// Elements
const loadCta = document.querySelector("#loadNew");
const output = document.querySelector("#output");

// URL to local file
const url = `./local-file.txt`;

loadCta.addEventListener("click", (e) => {
	if (output.innerHTML !== "") {
		output.innerHTML = "";
	}
	let xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4 && xhr.status === 200) {
			output.innerHTML = xhr.responseText;
		}
	};

	// Step 3 - Open up the XHR Request Object
	xhr.open("GET", url, true);

	// Step 4 - Send the XHR Request
	xhr.send();
});
