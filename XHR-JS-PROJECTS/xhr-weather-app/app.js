// XHR Weather API Project

// Elements
const myForm = document.querySelector("#weather-form");
const foreCastCta = document.querySelector(".forcast-cta");
const locationInput = document.querySelector("#location-input");
const topContent = document.querySelector(".top-content");
const bottomContent = document.querySelector(".bottom-content");
const currentContent = document.querySelector(".current-forecast");
const extendedContent = document.querySelector(".extended-forecast");

// API key
const key = `&key=HTRESPRGXHKMBVZ4CRYPBGVQU`;

// API URL
const baseUrl = `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/`;
const unitGroup = `unitGroup=us`;

// Event Listener for forecast CTA
foreCastCta.addEventListener("click", (e) => {
	e.preventDefault();
	const userLocation = locationInput.value.replace(/\s/g, "%20");
	if (userLocation !== "") {
		// Call function that make XHR Get Request and pass it updated URL
		const updatedUrl = `${baseUrl}${userLocation}?${unitGroup}${key}`;
		makeRequest(updatedUrl, locationInput.value);
		locationInput.value = "";
	} else {
		alert("Please Enter In City");
	}
});

// Function that makes an XHR Get request
const makeRequest = function (url, location) {
	const xhr = new XMLHttpRequest();
	xhr.onload = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			const data = JSON.parse(xhr.responseText);
			// Call function to render response data to the DOM
			renderData(data);
		} else {
			alert(
				`No results for "${location}". Try adding your city's state abbreviation to your search. Ex: 'your city, city's state'"`
			);
		}
	};
	xhr.open("GET", url, true);

	xhr.send();
};

// Function that renders received API data to the DOM
const renderData = function (data) {
	const { resolvedAddress, currentConditions, description, days } = data;
	const { conditions, feelslike, temp } = currentConditions;
	if (currentContent.innerHTML !== "") {
		currentContent.innerHTML = "";
	}
	if (extendedContent.innerHTML !== "") {
		extendedContent.innerHTML = "";
	}
	currentContent.classList.remove("hide");
	extendedContent.classList.remove("hide");

	currentContent.innerHTML += `
        <h2>Current Weather Conditions for <u>"${resolvedAddress}"</u> </h2>
        <h3> Weather Projection For Today: ${description}</h3>
        <h3>Temp: ${temp}&#8457</h3>
        <h3>Feels Like: ${feelslike}&#8457</h3>
        <h3>Sky Conditions: ${conditions} </h3>
        `;

	extendedContent.innerHTML += `<h2 class="extended-header"><u>Extended 15 Day Forecast</u></h2>`;
	days.forEach((day) => {
		const { datetime, tempmax, tempmin, temp, description } = day;
		let formatDate = datetime.split("-").reverse().join("-");
		extendedContent.innerHTML += `
        <div class="day">
                <h2>Date: ${formatDate}</h2>
                <h3>Weather Projection: ${description}</h3>
                <h3>Temp: ${temp}&#8457</h3>
                <h3>High Temp: ${tempmax}&#8457</h3>
                <h3>Low Temp: ${tempmin}&#8457</h3>
        </div>
        
        `;
	});
};
