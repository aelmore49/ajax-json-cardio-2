// XHR Parks API Project

const apiKey = `&api_key=RgncFGtyYPCPVbgpES5pOvlIXr4eOAgV7kJ0eMr4`;
const baseUrl = `https://developer.nps.gov/api/v1/parks`;

// Elements
const parkCta = document.querySelector(".park-cta");
const parkInput = document.querySelector("#park-input");
const parkForm = document.querySelector("#park-form");
const bottomContent = document.querySelector(".bottom-content");

// Event Listener for Park CTA
parkCta.addEventListener("click", (e) => {
	e.preventDefault();
	if (parkInput !== "") {
		const searchTerm = `?q=${parkInput.value}`;
		const updatedUrl = `${baseUrl}${searchTerm}${apiKey}`;

		// Call function to make an XHR Get Request and pass it the updated URL
		makeGetRequest(updatedUrl, parkInput.value);
	} else {
		alert("Please enter a search term");
	}
});

// Function that receives a URL and then make the approiate XHR Get Request with it.
const makeGetRequest = function (url, value) {
	const xhr = new XMLHttpRequest();

	xhr.onload = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			const data = JSON.parse(xhr.responseText);
			console.log(data);
			// Call Function that renders returned data to the DOM and it the parsed received data
			renderData(data, value);
			parkInput.value = "";
		} else {
			alert("network error");
		}
	};

	xhr.open("GET", url, true);
	xhr.send();
};

// Function that receive JSON parsed data and renders it to the DOM
const renderData = function (parkData, value) {
	if (bottomContent.innerHTML !== "") {
		bottomContent.innerHTML = "";
	}
	const { total, data } = parkData;
	if (data.length === 1) {
		const description = data[0].description;
		const fullName = data[0].fullName;

		bottomContent.innerHTML = `
        <h1>There are ${total} result(s) for "${value}"</h1>
        <h2>${fullName}</h2>
        <h3>${description} </h3>

        `;
	} else if (data.length > 1) {
		data.forEach((park) => {
			console.log(park);
		});
	} else {
		alert(`No results for "${value}". Try another search`);
	}
};
