// AJAX XHR Load User Project

//Elements
const loadCta = document.querySelector("#loadNew");
const output = document.querySelector("#output");

// API URL
const url = `https://randomuser.me/api/`;

// Event Listener for Load CTA
loadCta.addEventListener("click", (e) => {
	// If output element has pre-existing content, if so, clear it out
	if (output.innerHTML !== "") {
		output.innerHTML = "";
	}

	// Change CTA's text content once it is clicked
	loadCta.textContent = "Load Another User";

	// Make XHR Request
	// Step 1 - Create XHR Request Object
	let xhr = new XMLHttpRequest();

	// Step2 - Create onreadystatechange function
	xhr.onreadystatechange = function () {
		if (xhr.readyState === 4) {
			if (xhr.status === 200) {
				// Call renderData function and pass it the data
				renderData(JSON.parse(xhr.responseText));
			}
		}
	};

	// Step 3 - Open up the XHR Request Object
	xhr.open("GET", url, true);

	// Step 4 - Send the XHR Request
	xhr.send();
});

const renderData = function (data) {
	// Loop through data and display user info in output element
	data.results.forEach((info) => {
		console.log(info);
		output.innerHTML += `
                            <img src="${info.picture.large}" alt="${info.name.first}-${info.name.last}" >
							<h2>Name: ${info.name.first} ${info.name.last} </h2>
						    <h2>Age: ${info.dob.age}</h2>
							<h2>Gender: ${info.gender}</h2>
                            <h2>Email: ${info.email}</h2>
							<h2>Phone: ${info.phone}</h2> 
							<h2>City: ${info.location.city}</h2>
							<h2>State/Country: ${info.location.state}, ${info.location.country} </h2>`;
	});
};
