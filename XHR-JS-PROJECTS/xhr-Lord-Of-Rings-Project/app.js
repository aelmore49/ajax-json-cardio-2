// Lord Of The Rings XHR Project

const oneSubmit = document.querySelector(".one-submit");
const myForm = document.querySelector(".ring-form");
const bottomOutput = document.querySelector(".bottom-output");
const topOutput = document.querySelector(".top-output");
//API Specific constants
const token = `mGoS1cuIMtjEqlSmpZ5Y`;
const url = ` https://the-one-api.dev/v2/`;

// Event Listener for CTA
oneSubmit.addEventListener("click", (e) => {
	e.preventDefault();
	const userPick = myForm.elements["rings"].value.toLowerCase();
	const tempUrl = `${url}${userPick}`;
	getRequest(tempUrl, userPick);
});

// Function that makes an XHR Get Request
const getRequest = function (url, type) {
	const xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			console.log(JSON.parse(xhr.responseText));
			const data = JSON.parse(xhr.responseText);
			if (bottomOutput.innerHTML !== "") {
				bottomOutput.innerHTML = "";
			}
			renderData(data, type);
		}
	};

	xhr.open("GET", url, true);
	xhr.setRequestHeader("Authorization", `Bearer ${token}`);
	xhr.send();
};

// Function that renders response data to the DOM
const renderData = function (data, type) {
	if (type === `character`) {
		console.log(data);
		const docs = data.docs;
		console.log(docs);
		docs.forEach((doc) => {
			const { name, race, gender, wikiUrl } = doc;

			bottomOutput.innerHTML += `
                <h2> Name: ${name} </h2>
                <h2> Race: ${race} </h2>
                <h2> Gender: ${gender} </h2>
                <a href="${wikiUrl}" target="_blank">Learn More</a>
                <hr>
            `;
		});
	} else if (type === `book`) {
		console.log(data);
		const docs = data.docs;
		docs.forEach((doc) => {
			const { name } = doc;
			bottomOutput.innerHTML += `
                <h2> Name: ${name} </h2>
                <hr>
            `;
		});
	} else if (type === `movie`) {
		console.log(data);
		console.log(data);
		const docs = data.docs;
		docs.forEach((doc) => {
			const {
				name,
				runtimeInMinutes,
				budgetInMillions,
				academyAwardNominations,
				academyAwardWins,
				rottenTomatoesScore,
			} = doc;
			bottomOutput.innerHTML += `
                <h2> Name: ${name} </h2>
                <h2> Runtime In Minutes: ${runtimeInMinutes} </h2>
                <h2> Budget In Millions: ${budgetInMillions} </h2>
                <h2> Academy Award Nominations: ${academyAwardNominations} </h2>
                <h2> Academy Award Wins: ${academyAwardWins} </h2>
                <h2> Rotten Tomatoes Score: ${rottenTomatoesScore} </h2>
                <hr>
            `;
		});
	} else {
		console.log(data);
		console.log(data);
		const docs = data.docs;
		docs.forEach((doc) => {
			const { dialog, movie, character } = doc;
			bottomOutput.innerHTML += `
                <h2> Character: ${character} </h2>
                <h2> Movie: ${movie} </h2>
                <h2> Quote: "${dialog}" </h2>
                <hr>
            `;
		});
	}
};

// Function that creates and return an elmeent
function makeHtmlElement(
	parentElement,
	elementType,
	elementContent,
	elementClass
) {
	const element = document.createElement(elementType);
	element.innerHTML = elementContent;
	element.classList.add(elementClass);
	return parentElement.appendChild(element);
}
