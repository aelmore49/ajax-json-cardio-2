// AJAX XHR Load Chuck Norris Joke Project

// Elements
const jokeCta = document.querySelector("#loadNew");
const output = document.querySelector("#output");

// API URL
const url = `https://api.chucknorris.io/jokes/random`;

// Event Listener for Joke CTA
jokeCta.addEventListener("click", (e) => {
	if (output.innerHTML !== "") {
		output.innerHTML = "";
	}
	// Change text content of joke CTA after it is clicked
	jokeCta.textContent = "Load Another Joke";

	// XHR Object
	let xhr = new XMLHttpRequest();

	// XHR onreadystatechange function
	xhr.onreadystatechange = function () {
		if ((xhr.readyState === 4) & (xhr.status === 200)) {
			console.log(JSON.parse(xhr.responseText));
			// Call function to render joke and pass it the data
			renderJoke(JSON.parse(xhr.responseText));
		}
	};
	xhr.open("GET", url, true);
	xhr.send();
});

// Function to render joke
const renderJoke = function (data) {
	output.innerHTML = `
    <img src="${data.icon_url}" alt="chuck norris icon">
    <h2>${data.value}</h2>
    `;
};
