// XHR Cocktail API Project

// Elements
const cocktailCta = document.querySelector(".cocktail-cta");
const cocktailInput = document.querySelector("#cocktail-input");
const bottomOutput = document.querySelector(".bottom-content");

// API URL
const url = `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=`;

// Event Listener for cocktail cta
cocktailCta.addEventListener("click", (e) => {
	// Check input for value
	if (cocktailInput.value !== "") {
		const input = cocktailInput.value.toLowerCase();
		const tempUrl = `${url}${input}`;
		// Call function to setup GET request to API
		sendGetRequest(tempUrl, cocktailInput.value);
		cocktailInput.value = "";
	} else {
		alert("Please Enter A Cocktail Name To Search");
	}
});

// Function that send out an XHR Get Request
const sendGetRequest = function (url, searchItem) {
	// Setup XHR object
	const xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function () {
		if (xhr.status === 200 && xhr.readyState === 4) {
			const data = JSON.parse(xhr.responseText).drinks;
			console.log(data);
			// Call function to render data to the DOM
			if (data !== null) {
				renderData(data, searchItem);
			} else {
				alert(`No results for ${searchItem}`);
			}
		}
	};
	xhr.open("GET", url);
	xhr.send();
};

// Function that renders data to the DOM
const renderData = function (data, searchItem) {
	if (bottomOutput.innerHTML !== "") {
		bottomOutput.innerHTML = "";
	}
	bottomOutput.classList.remove("hide");
	if (data.length > 1) {
		bottomOutput.innerHTML += `<h2> ${data.length} results found for "${searchItem}"</h2>`;
		data.forEach((drink, index) => {
			const strDrink = drink.strDrink;
			const strAlcoholic = drink.strAlcoholic;
			const strCategory = drink.strCategory;
			const strDrinkThumb = drink.strDrinkThumb;
			const strInstructions = drink.strInstructions;
			const strIngredient1 = drink.strIngredient1;
			const strIngredient2 = drink.strIngredient2;
			const strIngredient3 = drink.strIngredient3;
			const strIngredient4 = drink.strIngredient4;
			const strIngredient5 = drink.strIngredient5;
			const strIngredient6 = drink.strIngredient6;
			const strIngredient7 = drink.strIngredient7;
			const strIngredient8 = drink.strIngredient8;
			const strIngredient9 = drink.strIngredient9;
			const strIngredient10 = drink.strIngredient10;
			const strIngredient11 = drink.strIngredient11;
			const strIngredient12 = drink.strIngredient12;
			const strIngredient13 = drink.strIngredient13;
			const strIngredient14 = drink.strIngredient14;
			const strIngredient15 = drink.strIngredient15;

			const strMeasure1 = drink.strMeasure1;
			const strMeasure2 = drink.strMeasure2;
			const strMeasure3 = drink.strMeasure3;
			const strMeasure4 = drink.strMeasure4;
			const strMeasure5 = drink.strMeasure5;
			const strMeasure6 = drink.strMeasure6;
			const strMeasure7 = drink.strMeasure7;
			const strMeasure8 = drink.strMeasure8;
			const strMeasure9 = drink.strMeasure9;
			const strMeasure10 = drink.strMeasure10;
			const strMeasure11 = drink.strMeasure11;
			const strMeasure12 = drink.strMeasure12;
			const strMeasure13 = drink.strMeasure13;
			const strMeasure14 = drink.strMeasure14;
			const strMeasure15 = drink.strMeasure15;

			bottomOutput.innerHTML += `
               
                <h3> Recipe ${index + 1} of ${data.length} </h3>
                <h3> Name: ${strDrink} </h3>
                <h3> Content: ${strAlcoholic} </h3>
                <h3> Category: ${strCategory} </h3>
				<h3> <u>Ingredients:</u><br>
				${strIngredient1 !== null ? `1: ${strIngredient1} - ` : ""}
				${strMeasure1 !== null ? `${strMeasure1} <br>` : ""}

				${strIngredient2 !== null ? `2: ${strIngredient2} - ` : ""}
				${strMeasure2 !== null ? `${strMeasure2} <br>` : ""}
				
				${strIngredient3 !== null ? `3: ${strIngredient3} -` : ""}
				${strMeasure3 !== null ? `${strMeasure3} <br>` : ""}

				${
					strIngredient4 !== null || strIngredient4 !== ""
						? `4: ${strIngredient4} -`
						: ""
				}
				${strMeasure4 !== null ? `${strMeasure4} <br>` : ""}

				${strIngredient5 !== null ? `5: ${strIngredient5} -` : ""}
				${strMeasure5 !== null ? `${strMeasure5} <br>` : ""}

				${strIngredient6 !== null ? `6: ${strIngredient6} -` : ""}
				${strMeasure6 !== null ? `${strMeasure6} <br>` : ""}

				${strIngredient7 !== null ? `7: ${strIngredient7} -` : ""}
				${strMeasure7 !== null ? `${strMeasure7} <br>` : ""}

				${strIngredient8 !== null ? `8: ${strIngredient8} -` : ""}
				${strMeasure8 !== null ? `${strMeasure8} <br>` : ""}

				${strIngredient9 !== null ? `9: ${strIngredient9} -` : ""}
				${strMeasure9 !== null ? `${strMeasure9} <br>` : ""}

				${strIngredient10 !== null ? `10: ${strIngredient10} -` : ""}
				${strMeasure10 !== null ? `${strMeasure10} <br>` : ""}

				${strIngredient11 !== null ? `11: ${strIngredient11} -` : ""}
				${strMeasure11 !== null ? `${strMeasure11} <br>` : ""}

				${strIngredient12 !== null ? `12: ${strIngredient12} -` : ""}
				${strMeasure12 !== null ? `${strMeasure12} <br>` : ""}

				${strIngredient13 !== null ? `13: ${strIngredient13} -` : ""}
				${strMeasure13 !== null ? `${strMeasure13} <br>` : ""}

				${strIngredient14 !== null ? `14: ${strIngredient14} -` : ""}
				${strMeasure14 !== null ? `${strMeasure14} <br>` : ""}

				${strIngredient15 !== null ? `15: ${strIngredient15} -` : ""}
				${strMeasure15 !== null ? `${strMeasure15} <br>` : ""}

				<h3> <u>Instructions:</u><br> ${strInstructions} </h3>
				
                <img src="${strDrinkThumb}" alt="image of ${strDrink}">
                <hr>


            `;
		});
	} else {
		bottomOutput.innerHTML += `<h2> 1 result found for "${searchItem}"</h2>`;
		const drinkData = data[0];
		const {
			strDrink,
			strAlcoholic,
			strCategory,
			strDrinkThumb,
			strInstructions,
		} = drinkData;

		bottomOutput.innerHTML += `
                <hr>
                <h3> Recipe 1 of 1 </h3>
                <h3> Name: ${strDrink} </h3>
                <h3> Content: ${strAlcoholic} </h3>
                <h3> Category: ${strCategory} </h3>
                <h3> Instructions: ${strInstructions} </h3>
                <img src="${strDrinkThumb}" alt="image of ${strDrink}">
                <hr>
            `;
	}
};
